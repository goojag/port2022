import '/styles/css/bootstrap.min.css';
import '/styles/scss/style.scss';

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
